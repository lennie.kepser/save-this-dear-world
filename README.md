# Introduction

(Please) Save This Dear World is a strategy simulation game in which you control little cloud people trying to create a better future.

![Hi there!](Screenshots/Screenshot%202023-04-21%20124052.png "Hi there!")

This is mostly a project for fun and an opportunity for me to get more aquainted with every aspect of Unreal Engine game development. Almost the entirety of the code, models, materials, shaders, animations and UI were created by me (Leonhard Kepser), with the exception of the low poly tree models (https://www.cgtrader.com/free-3d-models/exterior/landscape/low-poly-forest-nature-set-free-trial) and maybe some other small things I'm forgetting about. 

The proper version also includes the Ultra Dynamic Sky plugin for it's beautiful stunning weather effects as well as the Pyro VFX plugin for some fun explosion and fire effects. Both of these were .gitignored for this upload, as they are proprietary assets and the owners would probably not be too happy about them being available online. 

# Notes for cloning this repo

The project will (hopefully?) compile, but won't look quite as good as in the screenshots, as the Ultra Dynamic Sky plugin is missing. This also disables all of the dynamic time and weather functionality. You can press '.' in the editor to get additional funds.

![](Screenshots/Screenshot%202023-04-21%20121812.png)

# A brief overview

There are a couple things I want to highlight in STDW's code base and content directory, which I think are particularly neat.

## The terrain

The terrain material (which you can find in Content/Material/M_Terrain_New.uasset) is a bit unorthodox, because instead of using multiple layers of materials with different purposes (e.g. a sand layer, a mud layer, a grass layer), it uses one master material which changes color and roughness based on position and angle, naturally creating beaches, cliffs and fields. 

![Low Sealevel](Screenshots/Screenshot%202023-04-23%20234846.png)

While it has the nice effect of being a time saver when creating and modifying the terrain, it has the added benefit of allowing me to easily change the height of the beachline and cliffside, making it easy to modify the water levels like this:

![Medium Sealevel](Screenshots/Screenshot%202023-04-23%20234921.png)

It also just looks super nice especially the lowered roughness of the sand makes the subsurface area have some real nice colours.

## Custom Ability System

While I know that Unreal Engine comes with its own Ability System already in place, I figured making my own Stat and Ability System could be fun and educational. And I was right! For the most part. Making the class templates work for the stat system was a bit of a pain, but it worked out pretty nicely in the end.

There are three essential components to the ability system: Lil Guys, the abilities, and the stats. 

### Lil Guys

![](Screenshots/Screenshot%202023-04-21%20121126.png "A Lil' Guy")

Lil Guys are the central unit of the game, the player recruits them and can then use one of their abilities each turn/year. The header of the C++ part of the Lil Guys can be found in Source/Equation/Public/CP_LilGuyBase.h, and the Blueprint class that inherits from the C++ class and implements a lot of cosmetic and AI functionality can be found in Content/Blueprints/LilGuy/BP_LilGuy. The Blueprint class contains some basic logic for AI in a function called "WanderAround". This makes the Lil Guys randomly switch between walking around, sitting under trees, sleeping, collecting flowers and gifting these flowers to each other.



### Stats
The stats are the core feature of the gameplay in STDW, most abilities will either increase or decrease the stats of Lil Guys. The stats are attached to the main "LilGuy" class (which can be fund in Source/Equation/Public/CP_LilGuyBase.h), through a container class called "ULilGuyStatSet". All of the Stat classes can be found in Equation/AbilitySystem/Stats.These Stat classes are essentially wrappers around a value type (currently either float or bool) with some additional functionality. It might look like theres a lot of classes at first glance, but there's actually a fairly straight forward hierarchy in place here which makes it easy to implement new functionality for stats, or to add a stat with a new type of class as its value. 

For example, it would be trivial to add a stat that uses an Enum as its value, or an int instead of the default floats which most stats currently use.

![](Screenshots/Screenshot%202023-04-21%20122007.png)

The stat system is built on the base class "LilGuyStat" (see: LilGuyStat.h), which is a template class. The class hierarchy of e.g. LilGuyStatPercentage looks roughly like this:

LilGuyStatGeneral -> LilGuyStat<T> -> LilGuyStatNumeric<T> -> LilGuyStatClampe<T> -> LilGuyStatFloat -> LilGuyStatPercentage

With a bunch of classes branching of at a number of points. This is to give me a lot of flexibility when implementing new features for Stats. For example, LilGuyStatPercentage implements a new ToString function, which represents the float value as a more readable string value, useful for most stats.

### Abilities


The main goal and challenge of the ability system was to make their functionality as dynamic as possible, while still allowing me to easily modify the balancing through data tables/.csv's. This is achieved by having some data, like the cost of abilities, their associated props, their names and their magic numbers, available in a FTableRowBase derived class FAbilityRow (Source/AbilitySystem/AbilityRow.h) and their core functionality implemented through custom C++ classes, which are then linked in the FAbilityRow Data Table row through a TSubclassOf<AB_Base> class member.

![](Screenshots/abilites.mp4)

These ability classes all derive from the base class AB_Base, which essentially functions as an interface with some core functionality. A very important subclass of AB_Base is AB_Targeted, which implements a unit targeting feature, allowing the player to choose the Lil Guy that is the target for an ability. Most abilities are targeted  (e.g. Hand out flyer needs someone to hand the flyer to), so almost all the currently implemented abilities derive from AB_Targeted. AB_Targeted works by using a delegate that gets fired once the Lil Guy that used the ability has either succesfully walked to the target chosen by the player, or once the Lil Guy determins that it does not have a succesful path to the target unit. The effect fires either way, but the animations only execute if the lil guy succesfully made it to its target.
