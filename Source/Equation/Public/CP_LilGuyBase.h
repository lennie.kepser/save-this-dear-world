// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "CP_LilGuyBase.generated.h"

UENUM()
enum Specialty : int
{
	None = 0,
	Publicist,
	Researcher,
	Lobbyist,
	Activist
};

struct FAbilityRow;
class ULilGuyStatSet;

DECLARE_MULTICAST_DELEGATE_OneParam(FLilGuyArrivedDelegate, bool)


UCLASS()
class EQUATION_API ACP_LilGuyBase : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACP_LilGuyBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual ~ACP_LilGuyBase() override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public:
	/*
	 * Ability System Functions
	 */
	UFUNCTION(BlueprintCallable, Category="Ability System")
	void SelectSpecialty(Specialty val);

	UFUNCTION(BlueprintCallable, Category="Ability System")
	Specialty GetSpecialty();

	UFUNCTION(BlueprintImplementableEvent, Category="Ability System")
	void SpecialtySelectedEvent(Specialty Val);

	void ActivityActivated(const FAbilityRow& Val);
	
	UFUNCTION(BlueprintImplementableEvent, Category="Ability System")
	void SetChooseMode(bool Val);

	UFUNCTION(BlueprintCallable, Category="Ability System")
	const FString& GetUsedAbility();
	
	UFUNCTION(BlueprintCallable, Category="Ability System")
	void FireOnArriveDelegates(bool bSuccess);
	
	void SetBlockUnlocked(FString Name, bool Val);

	bool IsBlockUnlocked(FString Name, bool Default);

	FLilGuyArrivedDelegate ArrivedDelegate;

	UPROPERTY(BlueprintReadWrite, Category="Ability System")
	FString UsedAbilityThisTurn = "";
	
	UPROPERTY(BlueprintReadWrite, Category="Ability System")
	TEnumAsByte<Specialty> SelectedSpecialty;

	UPROPERTY()
	TMap<FString, bool> UnlockedBlocks;
public:
	/*
	 * Animations & Cosmetic Behaviour
	 */
	UFUNCTION(BlueprintImplementableEvent, Category="Behaviour")
	void GiveProp(TSubclassOf<AProp> Prop);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category="Behaviour")
	void HandPropTo(ACP_LilGuyBase* OtherGuy, TSubclassOf<AProp> Prop);
	
	UFUNCTION(BlueprintImplementableEvent, Category="Behaviour")
	void WalkToLilGuy(ACP_LilGuyBase* OtherGuy);
	
	UFUNCTION(BlueprintImplementableEvent, Category="Behaviour")
	void TalkTo(ACP_LilGuyBase* OtherGuy);

	UFUNCTION(BlueprintImplementableEvent, Category="Behaviour")
	void SetShirt(bool Val);

	
public:
	/*
	 * Gameplay Affecting Function & Events 
	 */
	void ResetTurn();
	
	UFUNCTION(BlueprintImplementableEvent, Category="Gameplay")
	void BPResetTurn();
	
	UFUNCTION(BlueprintImplementableEvent, Category="Gameplay")
	void SelectLilGuy();

	UFUNCTION(BlueprintCallable, Category="Gameplay")
	void Recruit();

	UFUNCTION(BlueprintCallable, Category="Gameplay")
	bool IsRecruited();
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Gameplay")
	ULilGuyStatSet* Stats;

	ULilGuyStatSet* GetStats() const
	{
		return Stats;
	}

public:
	// For delegate stuff
	void static RecruitInitialStatic(ACP_LilGuyBase* Val);
	void static SelectStatic(ACP_LilGuyBase* Val);
	

	
};
