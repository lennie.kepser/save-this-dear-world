﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "W_NewspaperArticleCPBase.generated.h"

/**
 * 
 */
UCLASS()
class EQUATION_API UW_NewspaperArticleCPBase : public UUserWidget
{
	GENERATED_BODY()
};
