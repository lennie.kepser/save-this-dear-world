// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "StringDataRow.generated.h"

/**
 * 
 */
USTRUCT()
struct EQUATION_API FStringDataRow : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Item Data Struct")
	FString Name;
	

};
