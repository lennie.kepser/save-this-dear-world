// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseClimateAffectedModel.h"
#include "BaseTree.generated.h"

/**
 * 
 */
UCLASS()
class EQUATION_API ABaseTree : public ABaseClimateAffectedModel
{
	GENERATED_BODY()
public:
	ABaseTree();

	
private:
	bool bIsPlanted;
};
