﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "ArticleRow.generated.h"

class UW_NewspaperArticleCPBase;
/**
 * 
 */
USTRUCT()
struct EQUATION_API FArticleRow : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Article Data Struct")
	FText Headline;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Article Data Struct")
	FText Body;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Article Data Struct")
	UTexture2D* Image;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Article Data Struct")
	int Priority;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Article Data Struct")
	bool bForceVisualContent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Article Data Struct")
	TSubclassOf<UUserWidget> ImageOverride;
};
