// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseClimateAffectedModel.generated.h"

UCLASS()
class EQUATION_API ABaseClimateAffectedModel : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseClimateAffectedModel();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	USceneComponent* SceneComponent;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UStaticMeshComponent* MeshComponent;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
	TArray<UStaticMesh*> Meshs;
	UPROPERTY(EditAnywhere)
	
	TArray<float> ProbabilityInOrder;

	UFUNCTION(BlueprintCallable)
	void SelectModel(float bias);

};
