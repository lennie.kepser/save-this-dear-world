// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "W_MainGameUIBase.generated.h"

UENUM(BlueprintType)
enum EMenus{
Overview,
Abilities,
Newspaper,
PickSomething
};

/**
 * 
 */
UCLASS()
class EQUATION_API UW_MainGameUIBase : public UUserWidget
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void OpenMenu(EMenus Val);
};
