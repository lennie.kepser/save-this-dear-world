// Copyright Epic Games, Inc. All Rights Reserved.

#include "Equation.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Equation, "Equation" );
