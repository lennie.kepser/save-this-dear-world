// Copyright Epic Games, Inc. All Rights Reserved.


#include "EquationGameModeBase.h"

#include "CP_LilGuyBase.h"
#include <cmath>


#include "Equation/AbilitySystem/Stats/LilGuyStatSet.h"
#include "NavigationSystem.h"
#include "Kismet/GameplayStatics.h"


void AEquationGameModeBase::SpawnLilGuy()
{
	FVector FoundSpawnPoint;
	bool Success = UNavigationSystemV1::K2_GetRandomReachablePointInRadius(GetWorld(),SpawnPoint, FoundSpawnPoint, 5000, NULL);
	ensure(Success);
	auto NewGuy = Cast<ACP_LilGuyBase>(GetWorld()->SpawnActor(LilGuyClass, &FoundSpawnPoint));
	LilGuys.Add(NewGuy);
}

TArray<ACP_LilGuyBase*>& AEquationGameModeBase::GetLilGuyArray()
{
	return LilGuys;
}

void AEquationGameModeBase::SetLilGuyChooseMode(bool Val)
{
	for (auto LilGuy : LilGuys)
	{
		
		LilGuy->SetChooseMode(Val);
	}
	if (Val){
	
	BPChooseLilGuy();
	}
}

void AEquationGameModeBase::InitialRecruitment(ACP_LilGuyBase* Chosen)
{
	Chosen->Recruit();
	Chosen->SelectLilGuy();
	SetLilGuyChooseMode(false);

	OnLilGuyChosen.Clear();
}

void AEquationGameModeBase::FireLilGuyChosenDelegate(ACP_LilGuyBase* Val)
{
	OnLilGuyChosen.Broadcast(Val);
	
}

void AEquationGameModeBase::StartNewGame()

{
	

	// Main Gameplay Variables
	CO2 = 0;

	Profitability = 1;
	Terrorism = 0;
	LeftRight = 0.4; // 1 = Left, 0 = Right
	RenewableEnergy = 0; //TBD
	FossilFuelEnergy = 800;
	Policy = 0;
	Year = 0;
	AP = 0;
	MaxConstructionPerYear = 100;
	SetLilGuyChooseMode(true);
	OnLilGuyChosen.AddUObject(this, &AEquationGameModeBase::InitialRecruitment);
	
	PostGameStart();
}



int AEquationGameModeBase::NextYear()
{
	const float YearSin = std::sin(Year/2);
	const float RandomFloat = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);

	
	float PollsSum = 0;
	float EnergyDemand = 0;
	float Pollution = 0;

	for (auto LilGuy : LilGuys)
	{
		LilGuy->GetStats()->Political += RandomFloat * 0.05 - 0.1;
		PollsSum += static_cast<float>(LilGuy->GetStats()->Political);
		LilGuy->GetStats()->EnergyDemand+= (LilGuy->GetStats()->EnvConsciousness * -4.0f + 2);
		EnergyDemand += static_cast<float>(LilGuy->GetStats()->EnergyDemand);
		Pollution += static_cast<float>(LilGuy->GetStats()->TransportationCO2);
	}
	
	Year += 1;
	const float UnfulfilledDemand = EnergyDemand - (FossilFuelEnergy + RenewableEnergy);
	
	float Polls = PollsSum / LilGuys.Num();
	
	if (Year % 5== 0)
	{
		LeftRight = Polls;
	}

	TArray<AActor*> LilGuyArray;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ACP_LilGuyBase::StaticClass(), LilGuyArray);
	for (auto Guy : LilGuyArray)
	{
		static_cast<ACP_LilGuyBase*>(Guy)->ResetTurn();
	}
	

	Policy += (LeftRight - Policy) / PolicyGrowthRate;
	Profitability = (1 - (Policy + Terrorism)/2)* (1- (Year / 50 * 0.5));

	float FossilFuelEnergyBuild = UnfulfilledDemand * Profitability;
	if (FossilFuelEnergyBuild > MaxConstructionPerYear)
	{
		FossilFuelEnergyBuild = MaxConstructionPerYear;
	}

	float RenewableEnergyBuild = UnfulfilledDemand * (1 - Profitability);
	if (RenewableEnergyBuild > MaxConstructionPerYear)
	{
		RenewableEnergyBuild = MaxConstructionPerYear;
	}

	FossilFuelEnergy += FossilFuelEnergyBuild;
	RenewableEnergy += RenewableEnergyBuild;

	CO2 += (FossilFuelEnergy + Pollution) / 100000;
	float Temperature = (CO2 * (1 + (1+ YearSin)*0.2)) + (RandomFloat/5) * 0.8;
	float EnviromentalFactor = Temperature;
	if (EnviromentalFactor <= 0)
	{
		EnviromentalFactor = 0;
	}

	CallEnviromentalController(EnviromentalFactor);

	BPCallPowerBuilder(FossilFuelEnergy, RenewableEnergy);
	CleanupPostTurn();
	PrintGameModeVariables();
	const float GuyAddition = (50 - static_cast<float>(Year))/120;
	if (std::floor(AmountOfLilGuys + GuyAddition) > std::floor(AmountOfLilGuys))
	{
		SpawnLilGuy();
	}
	AmountOfLilGuys+=GuyAddition;
	
	return Year;
}

int AEquationGameModeBase::GetYear(){
	return Year;
}

void AEquationGameModeBase::PrintGameModeVariables()
{
	UE_LOG(LogTemp, Log, TEXT("Profitability: %f"), Profitability);
	UE_LOG(LogTemp, Log, TEXT("CO2: %f"), CO2);
	UE_LOG(LogTemp, Log, TEXT("Terrorism: %f"), Terrorism);
	UE_LOG(LogTemp, Log, TEXT("RenewableEnergy: %f"), RenewableEnergy);
	UE_LOG(LogTemp, Log, TEXT("FossilFuelEnergy: %f"), FossilFuelEnergy);
	UE_LOG(LogTemp, Log, TEXT("Policy: %f"), Policy);
	UE_LOG(LogTemp, Log, TEXT("Amount of Lil Guys: %f"), AmountOfLilGuys);
}

void AEquationGameModeBase::CancelSelection()
{
OnLilGuyChosen.Clear();
	SetLilGuyChooseMode(false);
}

bool AEquationGameModeBase::TryPurchase(int Price)
{
	if (AP >= Price)
	{
		AP -= Price;
		BPPostPurchase();
		return true;
	}else
	{
		return false;
	}
}

int AEquationGameModeBase::GetAP()
{
	return AP;
}

void AEquationGameModeBase::SetAP(int Val)
{
	AP = Val;
	UpdateInfoPanel();
}


void AEquationGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	AmountOfLilGuys = 4;
	for (int i = 0; i < AmountOfLilGuys; i++)
	{
		SpawnLilGuy();
	}
	
}

void AEquationGameModeBase::SetLilGuy(ACP_LilGuyBase* Guy)
{
	CurrentlySelectedGuy = Guy;
}

ACP_LilGuyBase* AEquationGameModeBase::GetLilGuy()
{
	return CurrentlySelectedGuy;
}

