// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseClimateAffectedModel.h"


// Sets default values
ABaseClimateAffectedModel::ABaseClimateAffectedModel()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("<SCENE>"));
	SetRootComponent(SceneComponent);
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("<MODEL>"));
	
	MeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	SelectModel(0);


}

// Called when the game starts or when spawned
void ABaseClimateAffectedModel::BeginPlay()
{
	Super::BeginPlay();
	
}


// Called every frame
void ABaseClimateAffectedModel::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABaseClimateAffectedModel::SelectModel(float bias)
{
	const float Selection = bias + FMath::RandRange(0.0f, 1.0f);
	for (int i = ProbabilityInOrder.Num() - 1; i >= 0; --i)
	{
		if (Selection > ProbabilityInOrder[i])
		{
			MeshComponent->SetStaticMesh(Meshs[i]);
			const FRotator Rotator(0,FMath::RandRange(0,360), 0);
			MeshComponent->AddRelativeRotation(Rotator);
			break;
		}
	}
}

