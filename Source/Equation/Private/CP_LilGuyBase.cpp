// Fill out your copyright notice in the Description page of Project Settings.

#include "CP_LilGuyBase.h"

#include "Equation/AbilitySystem/AbilityRow.h"

#include "Equation/AbilitySystem/Stats/LilGuyStatSet.h"
#include "Equation/EquationGameModeBase.h"


// Sets default values
ACP_LilGuyBase::ACP_LilGuyBase(const FObjectInitializer& ObjectInitializer)
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SelectedSpecialty = None;
	Stats = CreateDefaultSubobject<ULilGuyStatSet>("STATS");
	Stats->SetupAttachment(RootComponent);
}

ACP_LilGuyBase::~ACP_LilGuyBase()
{
}

// Called when the game starts or when spawned
void ACP_LilGuyBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACP_LilGuyBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACP_LilGuyBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ACP_LilGuyBase::SelectSpecialty(Specialty val)
{
	SelectedSpecialty = val;
	SpecialtySelectedEvent(val);
}

Specialty ACP_LilGuyBase::GetSpecialty()
{
	return SelectedSpecialty;
}

void ACP_LilGuyBase::ResetTurn()
{
	UsedAbilityThisTurn = "";
	BPResetTurn();
}

void ACP_LilGuyBase::ActivityActivated(const FAbilityRow& Val)
{
	UsedAbilityThisTurn = Val.Name;
}

const FString& ACP_LilGuyBase::GetUsedAbility()
{
	return UsedAbilityThisTurn;
}

void ACP_LilGuyBase::SetBlockUnlocked(FString Name, bool Val)
{
	if (!UnlockedBlocks.Contains(Name))
	{
		UnlockedBlocks.Add(Name, Val);
	}else
	{
		UnlockedBlocks[Name] = Val;
	}
}

bool ACP_LilGuyBase::IsBlockUnlocked(FString Name, bool Default)
{
	if (!UnlockedBlocks.Contains(Name))
	{
		return Default;
	}else
	{
		return UnlockedBlocks[Name];
	}
	
}

void ACP_LilGuyBase::Recruit()
{
	Stats->bIsRecruited.SetVal(true);
	Stats->Support.SetVal(1.0f);
	const auto GameMode = Cast<AEquationGameModeBase>(GetWorld()->GetAuthGameMode());
	GameMode->FirstGuyRecruited = true;
	GameMode->RefreshAbilitiesMenu(this);
	SetShirt(true);
}

bool ACP_LilGuyBase::IsRecruited()
{
	return static_cast<bool>(Stats->bIsRecruited);
}

void ACP_LilGuyBase::FireOnArriveDelegates(bool bSuccess)
{
	ArrivedDelegate.Broadcast(bSuccess);
	ArrivedDelegate.Clear();
}

void ACP_LilGuyBase::RecruitInitialStatic(ACP_LilGuyBase* Val)
{
	Val->Recruit();
}

void ACP_LilGuyBase::SelectStatic(ACP_LilGuyBase* Val)
{
	Val->SelectLilGuy();
}

