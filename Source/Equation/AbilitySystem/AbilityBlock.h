// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilityRow.h"
#include "Engine/DataTable.h"
#include "AbilityBlock.generated.h"

/**
 * 
 */
USTRUCT()
struct EQUATION_API FAbilityBlock : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	FAbilityBlock();

	virtual ~FAbilityBlock() override;

	FAbilityRow* operator[](int i);

	int GetNum();

	FText GetName();

	int GetAPCost();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Item Data Struct")
	bool DefaultUnlocked = false;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Item Data Struct")
	UDataTable* AbilityTable;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Item Data Struct")
	FText Name;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Item Data Struct")
	int APCost;
};
