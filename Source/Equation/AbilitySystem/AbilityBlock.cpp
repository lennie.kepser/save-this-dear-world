// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilityBlock.h"


FText FAbilityBlock::GetName()
{
	return Name;
}

int FAbilityBlock::GetAPCost()
{
	return  APCost;
}

FAbilityBlock::FAbilityBlock() : APCost(0)
{
}

FAbilityBlock::~FAbilityBlock()
{
}

FAbilityRow* FAbilityBlock::operator[](int i)
{
	TArray<FAbilityRow*> Abilities;
	AbilityTable->GetAllRows("", Abilities);
	if (Abilities.Num() <= i)
	{
		UE_LOG(LogTemp, Error, TEXT("Activity Block index out of range"));
		return nullptr;
	}
	return Abilities[i];
}

int FAbilityBlock::GetNum()
{
	TArray<FAbilityRow*> Abilities;

	AbilityTable->GetAllRows("", Abilities);
	return Abilities.Num();
}
