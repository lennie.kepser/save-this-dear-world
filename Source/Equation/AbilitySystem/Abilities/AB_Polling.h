// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Equation/AbilitySystem/Abilities/AB_Prop.h"
#include "AB_Polling.generated.h"

class ILilGuyStatGeneral;
/**
 * 
 */
UCLASS()
class EQUATION_API UAB_Polling : public UAB_Prop
{
	GENERATED_BODY()
protected:
	virtual bool CanBeExecuted(ACP_LilGuyBase* Executor, ACP_LilGuyBase* Target, FAbilityRow& Data, FString& ErrorMessage, bool& ReturnToMenu) override;

	virtual void ArrivedAtTargetCallback(bool Success, ACP_LilGuyBase* Choice, ACP_LilGuyBase* Executor, FAbilityRow* Data) override;

	virtual void LilGuyChosenCallback(ACP_LilGuyBase* Choice, ACP_LilGuyBase* Executor, FAbilityRow* Data) override;

	static void UnlockStat(const ULilGuyStatSet& StatSet, ILilGuyStatGeneral& Stat);
};
