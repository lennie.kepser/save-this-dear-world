// Fill out your copyright notice in the Description page of Project Settings.


#include "AB_DonationDrive.h"

#include "Equation/AbilitySystem/AbilityRow.h"

#include "Equation/AbilitySystem/Stats/LilGuyStatSet.h"

#include "CP_LilGuyBase.h"
#include "Equation/EquationGameModeBase.h"



void UAB_DonationDrive::ArrivedAtTargetCallback(bool Success, ACP_LilGuyBase* Choice, ACP_LilGuyBase* Executor,
                                                FAbilityRow* Data)
{
	Super::ArrivedAtTargetCallback(Success, Choice, Executor, Data);
	const auto& Wealth = Choice->GetStats()->Wealth;
	const auto& Support = Choice->GetStats()->Support;
	const auto GameMode = Cast<AEquationGameModeBase>(GetWorld()->GetAuthGameMode());
	int Funds = GameMode->GetAP();
	Funds += std::ceil(Wealth * (Wealth * Support) * Data->Parameters[0]);
	GameMode->SetAP(Funds);
	if (Success)
	{
		Choice->GiveProp(GetProp(*Data));
		Choice->HandPropTo(Executor, GetProp(*Data));

	}
}
