// Fill out your copyright notice in the Description page of Project Settings.


#include "AB_Base.h"

#include "Equation/AbilitySystem/AbilityRow.h"
#include "Equation/EquationGameModeBase.h"



UAB_Base::UAB_Base()
{
}

UAB_Base::~UAB_Base()
{
}


bool UAB_Base::ExecuteFunction(FAbilityRow& Data, ACP_LilGuyBase* Executor)
{
	FString ErrorMessage;
	bool ReturnToMenu;
	if (!CanBeExecuted(Executor, nullptr, Data, ErrorMessage, ReturnToMenu))
	{
		return false;
	}
	UE_LOG(LogTemp, Warning, TEXT("Executed Ability, Class %s"),*GetClass()->GetName());
	return true;
}

AEquationGameModeBase* UAB_Base::GetGameMode()
{
	auto GameMode = Cast<AEquationGameModeBase>(GetWorld()->GetAuthGameMode());
	return GameMode;
}

bool UAB_Base::CanBeExecuted(ACP_LilGuyBase* Executor, ACP_LilGuyBase* Target, FAbilityRow& Data, FString& ErrorMessage, bool& ReturnToMenu)
{
	return (Executor != Target && GetGameMode()->GetAP() >= Data.FundCost);
}

void UAB_Base::ExecuteSuccessful(FAbilityRow& Data, ACP_LilGuyBase* Executor)
{
	Executor->ActivityActivated(Data);
	float AP = GetGameMode()->GetAP();
	AP -= Data.FundCost;
	GetGameMode()->SetAP(AP);
}
