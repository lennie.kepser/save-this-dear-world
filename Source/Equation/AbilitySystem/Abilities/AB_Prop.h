// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AB_Targeted.h"
#include "AB_Prop.generated.h"

class AProp;
/**
 * 
 */
UCLASS(Blueprintable)
class EQUATION_API UAB_Prop : public UAB_Targeted
{
	GENERATED_BODY()
protected:
	TSubclassOf<AProp> GetProp(FAbilityRow& Data);

	
};
