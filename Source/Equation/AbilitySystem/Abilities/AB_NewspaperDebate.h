// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Equation/AbilitySystem/Abilities/AB_Base.h"
#include "AB_NewspaperDebate.generated.h"

/**
 * 
 */
UCLASS()
class EQUATION_API UAB_NewspaperDebate : public UAB_Base
{
	GENERATED_BODY()
	
};
