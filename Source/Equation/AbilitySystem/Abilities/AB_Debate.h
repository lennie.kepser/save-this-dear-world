// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Equation/AbilitySystem/Abilities/AB_Targeted.h"
#include "AB_Debate.generated.h"

/**
 * 
 */
UCLASS()
class EQUATION_API UAB_Debate : public UAB_Targeted
{
	GENERATED_BODY()
	
	virtual void ArrivedAtTargetCallback(bool Success, ACP_LilGuyBase* Choice, ACP_LilGuyBase* Executor, FAbilityRow* Data) override;

};
