﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AB_Targeted.h"
#include "UObject/Object.h"
#include "AB_Recruit.generated.h"

/**
 * 
 */
UCLASS()
class EQUATION_API UAB_Recruit : public UAB_Targeted
{
	GENERATED_BODY()

	virtual bool CanBeExecuted(ACP_LilGuyBase* Executor, ACP_LilGuyBase* Target, FAbilityRow& Data, FString& ErrorMessage, bool& ReturnToMenu) override;

	virtual void ArrivedAtTargetCallback(bool Success, ACP_LilGuyBase* Choice, ACP_LilGuyBase* Executor, FAbilityRow* Data) override;

};
