﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "AB_Recruit.h"
#include "Equation/AbilitySystem/AbilityRow.h"

#include "Equation/AbilitySystem/Stats/LilGuyStatSet.h"

bool UAB_Recruit::CanBeExecuted(ACP_LilGuyBase* Executor, ACP_LilGuyBase* Target, FAbilityRow& Data,
                                FString& ErrorMessage, bool& ReturnToMenu)
{
	if (Target != nullptr && Target->IsRecruited())
	{
		ErrorMessage.Append("Can't target group member.");
		return false;
	}
	if (Target != nullptr && static_cast<float>(Target->GetStats()->Support) < Data.Parameters[0])
	{
		ErrorMessage.Append("Not enough Support.");
		return false;
	}
	return Super::CanBeExecuted(Executor, Target, Data, ErrorMessage,ReturnToMenu);
}

void UAB_Recruit::ArrivedAtTargetCallback(bool Success, ACP_LilGuyBase* Choice, ACP_LilGuyBase* Executor,
                                          FAbilityRow* Data)
{
	Super::ArrivedAtTargetCallback(Success, Choice, Executor, Data);
	Choice->Recruit();
	if (Success)
	{
		Executor->TalkTo(Choice);
	}
}
