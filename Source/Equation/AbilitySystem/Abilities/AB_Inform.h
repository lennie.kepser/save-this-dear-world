// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AB_Base.h"
#include "AB_Targeted.h"
#include "AB_Inform.generated.h"

/**
 * 
 */
UCLASS()
class EQUATION_API UAB_Inform : public UAB_Targeted
{
	GENERATED_BODY()
public:



	virtual void ArrivedAtTargetCallback(bool Success, ACP_LilGuyBase* Choice, ACP_LilGuyBase* Executor, FAbilityRow* Data) override;


};
