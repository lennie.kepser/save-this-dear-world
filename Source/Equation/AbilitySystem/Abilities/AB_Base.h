// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CP_LilGuyBase.h"
#include "AB_Base.generated.h"

class ACP_LilGuyBase;
class AEquationGameModeBase;
struct FAbilityRow;
/**
 * 
 */
UCLASS()
class EQUATION_API UAB_Base : public UObject
{
	GENERATED_BODY()
public:
	UAB_Base();
	virtual ~UAB_Base() override;
	
	virtual bool ExecuteFunction(FAbilityRow& Data, ACP_LilGuyBase* Executor);

	AEquationGameModeBase* GetGameMode();

	// Basic requirements for any ability execution
	virtual bool CanBeExecuted(ACP_LilGuyBase* Executor, ACP_LilGuyBase* Target, FAbilityRow& Data, FString& ErrorMessage, bool& ReturnToMenu);

	void ExecuteSuccessful(FAbilityRow& Data, ACP_LilGuyBase* Executor);
};
