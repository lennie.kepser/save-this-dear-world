// Fill out your copyright notice in the Description page of Project Settings.


#include "Equation/AbilitySystem/Abilities/AB_Debate.h"


#include "Equation/AbilitySystem/AbilityRow.h"

#include "Equation/AbilitySystem/Stats/LilGuyStatSet.h"

#include "CP_LilGuyBase.h"



void UAB_Debate::ArrivedAtTargetCallback(bool Success, ACP_LilGuyBase* Choice, ACP_LilGuyBase* Executor,
										 FAbilityRow* Data)
{
	Super::ArrivedAtTargetCallback(Success, Choice, Executor, Data);
	
	Executor->GetStats()->Reputation += Data->Parameters[0];
	
	Choice->GetStats()->ClampVaues();
	if (Success)
	{
		Executor->TalkTo(Choice);
		Choice->TalkTo(Executor);
	}
}

