// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AB_Base.h"
#include "CP_LilGuyBase.h"
#include "AB_Targeted.generated.h"

struct FAbilityRow;
/**
 * 
 */
UCLASS()
class EQUATION_API UAB_Targeted : public UAB_Base
{
	GENERATED_BODY()
public:
	virtual void LilGuyChosenCallback(ACP_LilGuyBase* Choice, ACP_LilGuyBase* Executor, FAbilityRow* Data);

	void SelectTarget(ACP_LilGuyBase* Executor, FAbilityRow& Data);

	virtual bool CanBeExecuted(ACP_LilGuyBase* Executor, ACP_LilGuyBase* Target, FAbilityRow& Data, FString& ErrorMessage, bool& ReturnToMenu) override;
	
	virtual void ArrivedAtTargetCallback(bool Success, ACP_LilGuyBase* Choice, ACP_LilGuyBase* Executor, FAbilityRow* Data);

	virtual bool ExecuteFunction(FAbilityRow& Data, ACP_LilGuyBase* Executor) override;
};
