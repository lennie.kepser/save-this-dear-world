// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AB_Base.h"
#include "AB_Prop.h"
#include "AB_Targeted.h"
#include "AB_FlyersBase.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class EQUATION_API UAB_FlyersBase : public UAB_Prop
{
	GENERATED_BODY()
public:
	
	virtual void LilGuyChosenCallback(ACP_LilGuyBase* Choice, ACP_LilGuyBase* Executor, FAbilityRow* Data) override;

	virtual void ArrivedAtTargetCallback(bool Success, ACP_LilGuyBase* Choice, ACP_LilGuyBase* Executor, FAbilityRow* Data) override;
	


	
};
