// Fill out your copyright notice in the Description page of Project Settings.


#include "AB_Prop.h"

#include "Equation/AbilitySystem/AbilityRow.h"

TSubclassOf<AProp> UAB_Prop::GetProp(FAbilityRow& Data)
{
	return Data.Prop;
}
