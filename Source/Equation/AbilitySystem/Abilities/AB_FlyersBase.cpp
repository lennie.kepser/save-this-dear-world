// Fill out your copyright notice in the Description page of Project Settings.
#include "AB_FlyersBase.h"

#include "Equation/AbilitySystem/AbilityRow.h"

#include "Equation/AbilitySystem/Stats/LilGuyStatSet.h"

#include "CP_LilGuyBase.h"


void UAB_FlyersBase::LilGuyChosenCallback(ACP_LilGuyBase* Choice, ACP_LilGuyBase* Executor, FAbilityRow* Data)
{
	Super::LilGuyChosenCallback(Choice, Executor, Data);
	Executor->GiveProp(GetProp(*Data));
}

void UAB_FlyersBase::ArrivedAtTargetCallback(bool Success, ACP_LilGuyBase* Choice, ACP_LilGuyBase* Executor,
                                             FAbilityRow* Data)
{
	Super::ArrivedAtTargetCallback(Success, Choice, Executor, Data);
	auto& Support = Choice->GetStats()->Support;
	auto& Reputation = Choice->GetStats()->Reputation;
	Support += Data->Parameters[0];
	Reputation -= Data->Parameters[1];


	Choice->GetStats()->ClampVaues();
	if (Success)
	{
		Executor->HandPropTo(Choice, GetProp(*Data));
		
	}
}

