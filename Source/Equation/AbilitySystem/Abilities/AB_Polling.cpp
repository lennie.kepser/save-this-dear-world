// Fill out your copyright notice in the Description page of Project Settings.


#include "Equation/AbilitySystem/Abilities/AB_Polling.h"
#include "Equation/AbilitySystem/Stats/LilGuyStat.h"
#include "Equation/AbilitySystem/Stats/LilGuyStatGeneral.h"
#include "Equation/AbilitySystem/Stats/LilGuyStatSet.h"
#include "Equation/AbilitySystem/AbilityRow.h"

bool UAB_Polling::CanBeExecuted(ACP_LilGuyBase* Executor, ACP_LilGuyBase* Target, FAbilityRow& Data,
                                FString& ErrorMessage, bool& ReturnToMenu)
{
	// Doesn't matter if the character is in the group or not, anyone can be polled. (Except yourself?)
	return UAB_Base::CanBeExecuted(Executor, Target, Data, ErrorMessage, ReturnToMenu);
}

ILilGuyStatGeneral* GetStatFromFloat(ULilGuyStatSet& StatSet, float Input)
{
	// Shitty solution, but I don't want to introduce another value for AbilityRow and I also don't want to make statset carry a map or something, so whatever
	switch(static_cast<int>(Input))
	{
	case 0:
		return &StatSet.Support;
	case 1:
		return &StatSet.EnvConsciousness;
	case 2:
		return &StatSet.Political;
	case 3:
		return &StatSet.Wealth;
	case 4:
		return &StatSet.Reputation;
	case 5:
		return &StatSet.TransportationCO2;
	case 6:
		return &StatSet.EnergyDemand;
	default:
		return nullptr;
	}
}

void UAB_Polling::ArrivedAtTargetCallback(bool Success, ACP_LilGuyBase* Choice, ACP_LilGuyBase* Executor,
                                          FAbilityRow* Data)
{
	UnlockStat(*Choice->Stats, *GetStatFromFloat(*Choice->Stats, Data->Parameters[0]));
	if (Success)
	{
		Choice->TalkTo(Executor);
	}
	Super::ArrivedAtTargetCallback(Success, Choice, Executor, Data);
	
}
void UAB_Polling::LilGuyChosenCallback(ACP_LilGuyBase* Choice, ACP_LilGuyBase* Executor, FAbilityRow* Data)
{
	Super::LilGuyChosenCallback(Choice, Executor, Data);
	Executor->GiveProp(GetProp(*Data));
}

void UAB_Polling::UnlockStat(const ULilGuyStatSet& StatSet, ILilGuyStatGeneral& Stat)
{
	auto EVisbility = EStatVisibility::Basic;
	if (static_cast<bool>(StatSet.bHasPDH))
	{
		EVisbility = EStatVisibility::Complete;
	}
	Stat.SetVisibility(EVisbility);
}



