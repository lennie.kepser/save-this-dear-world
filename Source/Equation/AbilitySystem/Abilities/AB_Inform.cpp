// Fill out your copyright notice in the Description page of Project Settings.

#include "AB_Inform.h"

#include "Equation/AbilitySystem/AbilityRow.h"

#include "Equation/AbilitySystem/Stats/LilGuyStatSet.h"

#include "CP_LilGuyBase.h"



void UAB_Inform::ArrivedAtTargetCallback(bool Success, ACP_LilGuyBase* Choice, ACP_LilGuyBase* Executor,
                                         FAbilityRow* Data)
{
	Super::ArrivedAtTargetCallback(Success, Choice, Executor, Data);
	
	auto& Support = Choice->GetStats()->Support;
	Support += Data->Parameters[0];
	
	Choice->GetStats()->ClampVaues();
	if (Success)
	{
		Executor->TalkTo(Choice);
	}
}

