// Fill out your copyright notice in the Description page of Project Settings.


#include "AB_Targeted.h"
#include "CP_LilGuyBase.h"
#include "Equation/EquationGameModeBase.h"


void UAB_Targeted::LilGuyChosenCallback(ACP_LilGuyBase* Choice, ACP_LilGuyBase* Executor,
                                        FAbilityRow* Data)
{
	FString ErrorMessage;
	bool ReturnToMenu;
	if (!CanBeExecuted(Executor, Choice, *Data, ErrorMessage, ReturnToMenu))
	{
		GetGameMode()->DisplayErrorMessageLilGuyChoice(ErrorMessage);
		return;
	}
	ExecuteSuccessful(*Data, Executor);

	GetGameMode()->OnLilGuyChosen.Clear();

	GetGameMode()->SetLilGuyChooseMode(false);

	Executor->SelectLilGuy();
	Executor->ArrivedDelegate.AddUObject(this, &UAB_Targeted::ArrivedAtTargetCallback, Choice, Executor, Data);
	Executor->WalkToLilGuy(Choice);
}

void UAB_Targeted::SelectTarget(ACP_LilGuyBase* Executor, FAbilityRow& Data)
{
	GetGameMode()->SetLilGuyChooseMode(true);
	GetGameMode()->OnLilGuyChosen.AddUObject(this,&UAB_Targeted::LilGuyChosenCallback, Executor, &Data);
}

bool UAB_Targeted::CanBeExecuted(ACP_LilGuyBase* Executor, ACP_LilGuyBase* Target, FAbilityRow& Data,
                                 FString& ErrorMessage, bool& ReturnToMenu)
{
	
	return Super::CanBeExecuted(Executor, Target, Data, ErrorMessage, ReturnToMenu);

}

void UAB_Targeted::ArrivedAtTargetCallback(bool Success, ACP_LilGuyBase* Choice, ACP_LilGuyBase* Executor,
                                           FAbilityRow* Data)
{
}

bool UAB_Targeted::ExecuteFunction(FAbilityRow& Data, ACP_LilGuyBase* Executor)
{
	if (!Super::ExecuteFunction(Data, Executor))
	{
		return false;
	}
	SelectTarget(Executor, Data);
	return true;
}

