﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilitySystem/Stats/LilGuyStatFloat.h"
#include <random>

FString LilGuyStatFloat::GetValAsString() const
{
	if (Visibility == Unknown)
	{
		return "?";
	}
	if (Visibility == Basic)
	{
		if (Val > (Max-Min) * 0.8)
		{
			return "Very High";
		}
		if (Val > (Max-Min) * 0.6)
		{
			return "High";
		}
		if (Val > (Max-Min) * 0.4)
		{
			return "Average";
		}
		if (Val > (Max-Min) * 0.2)
		{
			return "Low";
		}
		return "Very Low";
			
	}
	return FString::SanitizeFloat(std::round(Val));
	
}

LilGuyStatFloat::LilGuyStatFloat(float Val, float Min, float Max, float StdDiv, EStatVisibility Visibility,
	FString Name) : LilGuyStatClamped<float>(Val, Min, Max, Visibility, Name)
{
	static std::default_random_engine Generator = std::default_random_engine(time(0));

	std::normal_distribution<double> distribution(Val,(Max-Min) * StdDiv);
	this->Val = distribution(Generator);
}
