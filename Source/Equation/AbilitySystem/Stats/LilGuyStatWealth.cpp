﻿#include "AbilitySystem/Stats/LilGuyStatWealth.h"

LilGuyStatWealth::LilGuyStatWealth(float Val, float Min, float Max, float StdDiv, EStatVisibility Visibility,
	FString Name) : LilGuyStatFloat(Val,  Min, Max, StdDiv, Visibility, Name)
{
	// In the beginning of the game this should not be higher than half max, so it can increase over the course of the game
	if (this->Val > (Max - Min) * 0.5)
	{
		this->Val = (Max - Min) * 0.5;
	}
}

FString LilGuyStatWealth::GetValAsString() const
{
	if (Visibility == Basic)
	{
		if (Val > (Max-Min) * 0.75)
		{
			return "Billionaire";
		}
		if (Val > (Max-Min) * 0.5)
		{
			return "Millionaire";
		}
		if (Val > (Max-Min) * 0.4)
		{
			return "Upper Class";
		}
		if (Val > (Max-Min) * 0.3)
		{
			return "Upper Middle Class";
		}
		if (Val > (Max-Min) * 0.2)
		{
			return "Lower Middle Class";
		}
		if (Val > (Max-Min) * 0.1)
		{
			return "Lower Class";
		}
		return "Poor";
	}
	return LilGuyStatFloat::GetValAsString();
}
