// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "W_LilGuyStatsBase.generated.h"

class LilGuyStatFloat;
class UW_LilGuyStatBase;
class ACP_LilGuyBase;
/**
 * 
 */
UCLASS()
class EQUATION_API UW_LilGuyStatsBase : public UUserWidget
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	void Setup(ACP_LilGuyBase* LilGuy);

	void SetupWidget(LilGuyStatFloat& Stats);

	// The interface between the C++ LilGuyStat class and the BP widget class
	UFUNCTION(BlueprintImplementableEvent)
	TMap<FString, UW_LilGuyStatBase*> GetStatWidgets();
};
