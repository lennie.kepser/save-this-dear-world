﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Equation/AbilitySystem/Stats/LilGuyStatClamped.h"

/*
LilGuyStat::LilGuyStat(float Val, float Min, float Max, float StdDiv, EStatVisibility Visibility, FString Name) : Val(Val),
																												  Min(Min), Max(Max), Visibility(Visibility), Name(Name)
{
	static std::default_random_engine Generator = std::default_random_engine(time(0));

	std::normal_distribution<double> distribution(Val,(Max-Min) * StdDiv);
	distribution(Generator);
}
*/


template <class T>
LilGuyStatClamped<T>::LilGuyStatClamped(T Val, T Min, T Max, EStatVisibility Visibility, FString Name): LilGuyStatNumeric<T>(Val, Visibility, Name), Min(Min), Max(Max)
{
	Clamp();
}

template <class T>
LilGuyStatClamped<T>::~LilGuyStatClamped()
{
}

template <class T>
void LilGuyStatClamped<T>::Clamp()
{
	if (this->Val > Max)
	{
		this->Val = Max;
	}
	if (this->Val < Min)
	{
		this->Val = Min;
	}
}

template <class T>
void LilGuyStatClamped<T>::PostChange()
{
	LilGuyStat<T>::PostChange();
	Clamp();
}

template class LilGuyStatClamped<float>;
