// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "W_LilGuyStatBase.generated.h"


class LilGuyStatFloat;
/**
 * 
 */
UCLASS()
class EQUATION_API UW_LilGuyStatBase : public UUserWidget
{
	GENERATED_BODY()
public:

	// Right now we only need to show float stats. Can be expanded for others later
	void Setup(LilGuyStatFloat* Val);
	
	UFUNCTION(BlueprintImplementableEvent)
	void SetupText(const FString& Name, const FString& Val);
};
