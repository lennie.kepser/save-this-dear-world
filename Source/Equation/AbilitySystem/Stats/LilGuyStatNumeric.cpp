﻿#include "AbilitySystem/Stats/LilGuyStatNumeric.h"



template <class T>
LilGuyStatNumeric<T>& LilGuyStatNumeric<T>::operator+=(const T Other)
{
	Val+=Other;
	PostChange();
	return *this;
}

template <class T>
T LilGuyStatNumeric<T>::operator-(const LilGuyStatNumeric& Other) const
{
	return Val-Other.Val;
}

template <class T>
LilGuyStatNumeric<T>& LilGuyStatNumeric<T>::operator-=(const T Other)
{
	Val-=Other;
	PostChange();
	return *this;
}


template <class T>
T LilGuyStatNumeric<T>::operator*(const LilGuyStatNumeric& Other) const
{
	return Val*Other.Val;
}

template <class T>
T LilGuyStatNumeric<T>::operator*(const T& Other) const
{
	return Val*Other;
}

template <class T>
T LilGuyStatNumeric<T>::operator+(const T& Other) const
{
	return Val + Other;
}

template <class T>
LilGuyStatNumeric<T>& LilGuyStatNumeric<T>::operator*=(const T Other)
{
	Val*=Other;
	PostChange();
	return *this;
}

template <class T>
T LilGuyStatNumeric<T>::operator+(const LilGuyStatNumeric& Other) const
{
	return Val+Other.Val;
}

template class LilGuyStatNumeric<float>;
