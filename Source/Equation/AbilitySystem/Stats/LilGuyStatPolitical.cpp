﻿#include "AbilitySystem/Stats/LilGuyStatPolitical.h"

LilGuyStatPolitical::LilGuyStatPolitical(float Val, float Min, float Max, float StdDiv, EStatVisibility Visibility,
	FString Name) : LilGuyStatFloat(Val, Min, Max, StdDiv, Visibility, Name)
{
}

FString LilGuyStatPolitical::GetValAsString() const
{
	if (Visibility == Basic)
	{
		if (Val > (Max-Min) * 0.66)
		{
			return "Left";
		}
		if (Val > (Max-Min) * 0.33)
		{
			return "Center";
		}
		return "Right";
	}
	if (Visibility == Complete)
	{
		if (Val > (Max-Min) * 0.8)
		{
			return "Far Left";
		}
		if (Val > (Max-Min) * 0.6)
		{
			return "Center Left";
		}
		if (Val > (Max-Min) * 0.4)
		{
			return "Center";
		}
		if (Val > (Max-Min) * 0.)
		{
			return "Center Right";
		}
		return "Far Right";
	}
	return LilGuyStatFloat::GetValAsString();
}
