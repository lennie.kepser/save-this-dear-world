﻿#pragma once
#include "AbilitySystem/Stats/LilGuyStatFloat.h"

class LilGuyStatPolitical : public LilGuyStatFloat
{
public:
	LilGuyStatPolitical(float Val, float Min, float Max, float StdDiv, EStatVisibility Visibility, FString Name);
	virtual FString GetValAsString() const override;

};
