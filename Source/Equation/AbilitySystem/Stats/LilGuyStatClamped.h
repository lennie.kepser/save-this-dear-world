﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AbilitySystem/Stats/LilGuyStatNumeric.h"

/**
 * 
 */
template<class T>
class LilGuyStatClamped : public LilGuyStatNumeric<T>
{
public:
	LilGuyStatClamped(T Val, T Min, T Max, EStatVisibility Visibility, FString Name);

	virtual ~LilGuyStatClamped() override;
	
	void Clamp();

	virtual void PostChange() override;

protected:
	T Min;
	T Max;
};
