// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilitySystem/Stats/W_LilGuyStatsBase.h"

#include "CP_LilGuyBase.h"
#include "AbilitySystem/Stats/LilGuyStatSet.h"
#include "AbilitySystem/Stats/W_LilGuyStatBase.h"



void UW_LilGuyStatsBase::Setup(ACP_LilGuyBase* LilGuy)
{
	auto Stats = LilGuy->GetStats();
	SetupWidget(Stats->Support);
	SetupWidget(Stats->EnvConsciousness);
	SetupWidget(Stats->Political);
	SetupWidget(Stats->Wealth);
	SetupWidget(Stats->Reputation);
	SetupWidget(Stats->TransportationCO2);
	SetupWidget(Stats->EnergyDemand);
}

void UW_LilGuyStatsBase::SetupWidget(LilGuyStatFloat& Stat)
{
	auto StatWidgets = GetStatWidgets();
	if (ensure(StatWidgets.Contains(Stat.GetName())))
	{
		StatWidgets[Stat.GetName()]->SetupText(Stat.GetName(), Stat.GetValAsString());

	}else
	{
		UE_LOG(LogTemp, Error, TEXT("%s not found in Map"), *Stat.GetName())
	}

}
