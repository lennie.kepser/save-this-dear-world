﻿#pragma once
#include "AbilitySystem/Stats/LilGuyStat.h"

extern template class LilGuyStat<bool>;

class LilGuyStatBool : public LilGuyStat<bool>
{
public:
	LilGuyStatBool(bool Val, EStatVisibility Visibility, FString Name);

	virtual ~LilGuyStatBool() override;

	virtual FString GetValAsString() const override;

};
