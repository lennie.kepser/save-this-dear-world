﻿#include "AbilitySystem/Stats/LilGuyStatBool.h"

LilGuyStatBool::LilGuyStatBool(bool Val, EStatVisibility Visibility, FString Name) : LilGuyStat<bool>(Val, Visibility, Name)
{
}

LilGuyStatBool::~LilGuyStatBool()
{
}

FString LilGuyStatBool::GetValAsString() const
{
	if (Val)
	{
		return "True";
	}
	return "False";
}
