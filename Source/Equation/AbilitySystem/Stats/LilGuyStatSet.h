// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystem/Stats/LilGuyStatBool.h"
#include "AbilitySystem/Stats/LilGuyStatPolitical.h"
#include "AbilitySystem/Stats/LilGuyStatWealth.h"

#include "AbilitySystem/Stats/LilGuyStatPercentage.h"
#include "LilGuyStatSet.generated.h"



	
/**
 * 
 */
UCLASS()
class EQUATION_API ULilGuyStatSet : public USceneComponent
{
	GENERATED_BODY()
	
public:
	ULilGuyStatSet();

	virtual void BeginPlay() override;

	void ClampVaues();

public: 
	LilGuyStatPercentage Support = LilGuyStatPercentage(0.5, 0, 1, 0.25, Basic, "Support");
	LilGuyStatPercentage EnvConsciousness = LilGuyStatPercentage(0, 0, 0.25, 1, Unknown, "Awareness");
	LilGuyStatPercentage Reputation = LilGuyStatPercentage(1, 0, 1, 0.25, Unknown, "Reputation");

	LilGuyStatFloat TransportationCO2 = LilGuyStatFloat(40, 0, 50, 0.10, Unknown, "Transportation CO2");
	LilGuyStatFloat EnergyDemand = LilGuyStatFloat(200,  100, 250, 0.01, Unknown, "Energy Demand");;
	LilGuyStatWealth Wealth = LilGuyStatWealth(0.5, 0, 1, 0.25, Unknown, "Wealth");
	LilGuyStatPolitical Political = LilGuyStatPolitical(0.5,0,0.125,1, Unknown, "Politics");
	LilGuyStatBool bHasPDH = LilGuyStatBool(false, Unknown, "PHD");
	LilGuyStatBool bIsRecruited = LilGuyStatBool(false, Unknown, "Recruited");
	
	
	/*
	GAMEPLAY_STAT(Support, 0.5, 0, 1, 0.25, Basic);
	GAMEPLAY_STAT(EnvConsciousness, 0, 0, 0.25, 1, Unknown);
	GAMEPLAY_STAT(Political, 0.5, 0, 0.25, 1, Unknown);
	GAMEPLAY_STAT(Wealth, 0.5, 0, 1, 0.25, Unknown);
	GAMEPLAY_STAT(Reputation, 0.5, 0, 1, 0.25, Unknown);
	GAMEPLAY_STAT(TransportationCO2, 40, 0, 50, 0.10, Unknown);
	GAMEPLAY_STAT(EnergyDemand, 200, 100, 250, 0.01, Unknown);
	*/


	
};
