﻿#pragma once
#include "AbilitySystem/Stats/LilGuyStatGeneral.h"

template <typename T>
class LilGuyStat : public ILilGuyStatGeneral
{
public:
	
	LilGuyStat(T Val, EStatVisibility Visibility, FString Name);

	virtual ~LilGuyStat() override;
	
	virtual FString GetValAsString() const override
	{
		return "ERROR";
	}

	explicit operator T() const { return Val; };

	virtual const FString& GetName() override
	{
		return Name;
	}

	void SetVal(const T& Other);

	virtual void SetVisibility(TEnumAsByte<EStatVisibility> NewVal) override;
	
protected:
	virtual void PostChange() {};
	
	T Val;
	
	TEnumAsByte<EStatVisibility> Visibility;
	FString Name;

};


