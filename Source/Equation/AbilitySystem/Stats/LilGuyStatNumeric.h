﻿#pragma once
#include "AbilitySystem/Stats/LilGuyStat.h"

template <class T>
class LilGuyStatNumeric : public LilGuyStat<T>
{
public:
	LilGuyStatNumeric(T Val, EStatVisibility Visibility, FString Name) : LilGuyStat<T>(Val, Visibility, Name) {}
	
	T operator*(const LilGuyStatNumeric& Other) const;
	
	T operator*(const T& Other) const;
	
	T operator+(const T& Other) const;

	T operator-(const LilGuyStatNumeric& Other) const;

	T operator+(const LilGuyStatNumeric& Other) const;

	LilGuyStatNumeric& operator*=(const T Other);
	
	LilGuyStatNumeric& operator+=(const T Other);
	
	LilGuyStatNumeric& operator-=(const T Other);

};
