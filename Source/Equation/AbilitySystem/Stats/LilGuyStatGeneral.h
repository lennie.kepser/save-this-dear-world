﻿#pragma once

UENUM()
enum EStatVisibility : int
{
	Unknown = 0,
	Basic,
	Complete
};

//Abstract class for interfacing with LilGuyStats without a specified val type
class ILilGuyStatGeneral
{
public:
	virtual ~ILilGuyStatGeneral() {};

	virtual FString GetValAsString() const = 0;
	virtual const FString& GetName() = 0;
	virtual void SetVisibility(TEnumAsByte<EStatVisibility> NewVal) = 0;

protected:
	
};
