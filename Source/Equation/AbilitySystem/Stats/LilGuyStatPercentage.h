﻿#pragma once
#include "AbilitySystem/Stats/LilGuyStatFloat.h"

class LilGuyStatPercentage : public LilGuyStatFloat
{
public:

	LilGuyStatPercentage(float Val, float Min, float Max, float StdDiv, EStatVisibility Visibility, FString Name);
	virtual FString GetValAsString() const override;
};
