﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AbilitySystem/Stats/LilGuyStatClamped.h"

extern template class LilGuyStatClamped<float>;

/**
 * 
 */
class LilGuyStatFloat : public LilGuyStatClamped<float>
{

public:
	LilGuyStatFloat(float Val, float Min, float Max, float StdDiv, EStatVisibility Visibility, FString Name);

	virtual ~LilGuyStatFloat() override {};
	
	virtual FString GetValAsString() const override;
};
