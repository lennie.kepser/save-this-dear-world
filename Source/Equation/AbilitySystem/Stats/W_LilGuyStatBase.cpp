// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilitySystem/Stats/W_LilGuyStatBase.h"

#include "AbilitySystem/Stats/LilGuyStatFloat.h"


void UW_LilGuyStatBase::Setup(LilGuyStatFloat* Val)
{
	SetupText(Val->GetName(), Val->GetValAsString());
}
