// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilitySystem/Stats/LilGuyStatSet.h"


ULilGuyStatSet::ULilGuyStatSet()
{
}

void ULilGuyStatSet::BeginPlay()
{
	Super::BeginPlay();
	// Initializing objects and setting default values
	/*
	Support = LilGuyStatPercentage(0.5, 0, 1, 0.25, Basic, "Support");
	EnvConsciousness = LilGuyStatPercentage(0, 0, 0.25, 1, Unknown, "Environmental Consciousness");
	Reputation = LilGuyStatPercentage(1, 0, 1, 0.25, Unknown, "Reputation");
	TransportationCO2 = LilGuyStatFloat(40, 0, 50, 0.10, Unknown, "Transportation CO2");
	EnergyDemand = LilGuyStatFloat(200,  100, 250, 0.01, Unknown, "Energy Demand");
	Wealth = LilGuyStatWealth(0.5, 0, 1, 0.25, Unknown, "Wealth");
	Political = LilGuyStatPolitical(0.5,0,0.125,1, Unknown, "Politics");
	bHasPDH = LilGuyStatBool(false, Unknown, "PHD");
	bIsRecruited = LilGuyStatBool(false, Unknown, "Recruited");
	*/
	
}


void ULilGuyStatSet::ClampVaues()
{
	Support.Clamp();
	Reputation.Clamp();
	EnvConsciousness.Clamp();
	Political.Clamp();
	Wealth.Clamp();
	TransportationCO2.Clamp();
	EnergyDemand.Clamp();
}
