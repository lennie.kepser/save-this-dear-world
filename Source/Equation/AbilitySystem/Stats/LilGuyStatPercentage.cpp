﻿#include "AbilitySystem/Stats/LilGuyStatPercentage.h"

LilGuyStatPercentage::LilGuyStatPercentage(float Val, float Min, float Max, float StdDiv, EStatVisibility Visibility, FString Name) : LilGuyStatFloat(Val, Min, Max, StdDiv, Visibility, Name)
{
}

FString LilGuyStatPercentage::GetValAsString() const
{
	if (Visibility == Complete)
	{
		return FString::SanitizeFloat(std::round(Val*1000.0)/10.0) + "%";
	}
	return LilGuyStatFloat::GetValAsString();
}
