﻿
#include "AbilitySystem/Stats/LilGuyStat.h"



template <class T>
LilGuyStat<T>::LilGuyStat(T Val, EStatVisibility Visibility, FString Name) : Val(Val), Visibility(Visibility), Name(Name)
{
	
}


template <class T>
LilGuyStat<T>::~LilGuyStat()
{
	
}


template <class T>
void LilGuyStat<T>::SetVisibility(TEnumAsByte<EStatVisibility> NewVal)
{
	Visibility = NewVal;	
}


template <typename T>
void LilGuyStat<T>::SetVal(const T& Other)
{
	Val = Other;
	PostChange();
}


template class LilGuyStat<bool>;
template class LilGuyStat<float>;
