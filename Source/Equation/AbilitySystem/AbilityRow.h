// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Engine/DataTable.h"
#include "CoreMinimal.h"
#include "AbilityRow.generated.h"

class UW_NewspaperArticleCPBase;
class UAB_Base;
class AProp;

/**
 * 
 */
USTRUCT()
struct EQUATION_API FAbilityRow : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	FAbilityRow();
	virtual ~FAbilityRow() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Ability Data Struct")
	TSubclassOf<class UAB_Base> AbilityClass;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Ability Data Struct")
	TArray<float> Parameters;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Ability Data Struct")
	FString Name;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Ability Data Struct")
	FString ToolTipText;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Ability Data Struct")
	int FundCost;

	// Prop to be used in the ability (e.g. a flyer thats handed out)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Ability Data Struct")
	TSubclassOf<AProp> Prop;

	// Name of article in ability article data table
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Ability Data Struct")
	FString ArticleName;
};
