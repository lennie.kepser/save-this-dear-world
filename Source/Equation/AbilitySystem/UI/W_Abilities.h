// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Blueprint/UserWidget.h"
#include "W_Abilities.generated.h"

class ACP_LilGuyBase;
enum Specialty;
/**
 * 
 */
UCLASS()
class EQUATION_API UW_Abilities : public UUserWidget
{
	GENERATED_BODY()
	
	public:
	UPROPERTY(BlueprintReadWrite)
	ACP_LilGuyBase* AffectedGuy;

	UFUNCTION(BlueprintCallable)
	void SelectSpecialty(Specialty Val);
}; 
