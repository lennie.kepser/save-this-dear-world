// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "W_ActivityBlockBase.generated.h"

class ACP_LilGuyBase;
struct FAbilityBlock;

/**
 * 
 */
UCLASS()
class EQUATION_API UW_ActivityBlockBase : public UUserWidget
{
	GENERATED_BODY()

public:
	
	void Setup(FAbilityBlock* DataVal);
	
	virtual ~UW_ActivityBlockBase() override;

	UFUNCTION(BlueprintCallable)
	FText GetName();

	UFUNCTION(BlueprintCallable)
	int GetRowAmount();

	UFUNCTION(BlueprintCallable)
	FText GetAPCostText();

	UFUNCTION(BlueprintCallable)
	int GetAPCost();
	
	UFUNCTION(BlueprintCallable)
	void InitializeActivity(UW_ActivityBase* Activity, int i);

	UFUNCTION(BlueprintImplementableEvent)
	void SetupVisuals();

	UFUNCTION(BlueprintCallable)
	bool GetUnlocked(ACP_LilGuyBase* Guy);

	UFUNCTION(BlueprintCallable)
	void SetUnlocked(bool Val,ACP_LilGuyBase* Guy);

private:
	FAbilityBlock* Data;
	
	
};
