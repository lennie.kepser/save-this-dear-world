// Fill out your copyright notice in the Description page of Project Settings.


#include "W_ActivityBlockBase.h"
#include "CP_LilGuyBase.h"
#include "Equation/AbilitySystem/AbilityBlock.h"
#include "W_ActivityBase.h"


void UW_ActivityBlockBase::Setup(FAbilityBlock* DataVal)
{
	Data = DataVal;
	SetupVisuals();
}

UW_ActivityBlockBase::~UW_ActivityBlockBase()
{
}

FText UW_ActivityBlockBase::GetName()
{
	return Data->GetName();
}

int UW_ActivityBlockBase::GetRowAmount()
{
	return Data->GetNum();
}

FText UW_ActivityBlockBase::GetAPCostText()
{
	return FText::Format(FText::FromString("{0} AP"), Data->GetAPCost());
}

int UW_ActivityBlockBase::GetAPCost()
{
	return Data->GetAPCost();
}


void UW_ActivityBlockBase::InitializeActivity(UW_ActivityBase* Activity, int i)
{
	
	Activity->SetActivity((*Data)[i]);
}

void UW_ActivityBlockBase::SetUnlocked(bool Val, ACP_LilGuyBase* Guy)
{
	Guy->SetBlockUnlocked(GetName().ToString(), Val);
}

bool UW_ActivityBlockBase::GetUnlocked(ACP_LilGuyBase* Guy)
{
	return Guy->IsBlockUnlocked(GetName().ToString(),Data->DefaultUnlocked);
	
}
