// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "W_ActivityBlockBase.h"
#include "Engine/DataTable.h"

#include "Blueprint/UserWidget.h"
#include "W_ActivityPanelBase.generated.h"

/**
 * 
 */
UCLASS()
class EQUATION_API UW_ActivityPanelBase : public UUserWidget
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	void InitializeActivityBlock(UW_ActivityBlockBase* Block, FName Name);

	UFUNCTION(BlueprintCallable)
	void UnlockPanel();
	
	UFUNCTION(BlueprintCallable)
	TArray<FName> GetNameArray();
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UDataTable* ActivityTable;
};
