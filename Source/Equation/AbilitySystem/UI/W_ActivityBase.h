// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "W_ActivityBase.generated.h"

class UAB_Base;
struct FAbilityRow;

/**
 * 
 */
UCLASS()
class EQUATION_API UW_ActivityBase : public UUserWidget
{
	GENERATED_BODY()

public:

	virtual ~UW_ActivityBase() override;

	void SetActivity(FAbilityRow* Row);
	
	UFUNCTION(BlueprintCallable)
	FText GetActivityName();

	UFUNCTION(BlueprintCallable)
	FText GetActivityAPCost();

	UFUNCTION(BlueprintCallable)
	float GetAPCost();

	UFUNCTION(BlueprintImplementableEvent)
	void SetupVisuals();

	UFUNCTION(BlueprintImplementableEvent)
	AEquationGameModeBase* GetMyGameMode();

	UFUNCTION(BlueprintCallable)
	bool ActivateEffect();
	
	FAbilityRow* Data;

private:
	UPROPERTY()
	UAB_Base* MyAbility;
};
