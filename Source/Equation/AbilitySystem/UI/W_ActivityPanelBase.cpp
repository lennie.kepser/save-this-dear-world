// Fill out your copyright notice in the Description page of Project Settings.


#include "W_ActivityPanelBase.h"
#include "Equation/AbilitySystem/AbilityBlock.h"

// Returns whether the block is unlocked
void UW_ActivityPanelBase::InitializeActivityBlock(UW_ActivityBlockBase* Block, FName Name)
{
	auto row = ActivityTable->FindRow<FAbilityBlock>(Name, "", true);
	Block->Setup(row);
	
}

void UW_ActivityPanelBase::UnlockPanel()
{
	// Not implemented
}

TArray<FName> UW_ActivityPanelBase::GetNameArray()
{
	return ActivityTable->GetRowNames();
}
