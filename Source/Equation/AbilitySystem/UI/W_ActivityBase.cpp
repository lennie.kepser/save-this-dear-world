// Fill out your copyright notice in the Description page of Project Settings.
#include "W_ActivityBase.h"
#include "Equation/AbilitySystem/Abilities/AB_Base.h"
#include "Equation/AbilitySystem/AbilityRow.h"
#include "Equation/EquationGameModeBase.h"

#define LOCTEXT_NAMESPACE "FundText"


UW_ActivityBase::~UW_ActivityBase()
{
	
}

void UW_ActivityBase::SetActivity(FAbilityRow* Row)
{
	UE_LOG(LogTemp, Log, TEXT("Creating Ability of Class %s"),*Row->AbilityClass->GetName())
	MyAbility = NewObject<UAB_Base>(this, Row->AbilityClass);
	Data = Row;
	SetupVisuals();
}

FText UW_ActivityBase::GetActivityName()
{
	return FText::FromString(Data->Name);
}

FText UW_ActivityBase::GetActivityAPCost()
{
	if (Data->FundCost == 0)
	{
		return FText::FromString("");
	}
	return FText::Format(LOCTEXT("FundText","{0} Funds"),Data->FundCost);
}

float UW_ActivityBase::GetAPCost()
{
	return Data->FundCost;
}

bool UW_ActivityBase::ActivateEffect()
{
	return MyAbility->ExecuteFunction(*Data, GetMyGameMode()->GetLilGuy());
}


