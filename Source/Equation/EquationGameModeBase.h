// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once
#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "EquationGameModeBase.generated.h"

class ACP_LilGuyBase;
DECLARE_MULTICAST_DELEGATE_OneParam(FLilGuyChosenDelegate, ACP_LilGuyBase* /* Choice */)


/**
 * 
 */
UCLASS()
class EQUATION_API AEquationGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintImplementableEvent)
	void RefreshAbilitiesMenu(ACP_LilGuyBase* LilGuy);

	UFUNCTION(BlueprintCallable)
	void SetLilGuyChooseMode(bool Val);
	
	void InitialRecruitment(ACP_LilGuyBase* Chosen);

	UFUNCTION(BlueprintCallable)
	void FireLilGuyChosenDelegate(ACP_LilGuyBase* Val);

	UFUNCTION(BlueprintImplementableEvent)
	void BPChooseLilGuy();

	UFUNCTION(BlueprintCallable)
	void StartNewGame();
	
	UFUNCTION(BlueprintImplementableEvent)
	void UpdateInfoPanel();

	UFUNCTION(BlueprintImplementableEvent)
	void DisplayErrorMessageLilGuyChoice(const FString& ErrorMessage);
	
	UFUNCTION(BlueprintCallable)
	int NextYear();

	UFUNCTION(BlueprintImplementableEvent)
	void CallEnviromentalController(float Fuckedness);

	UFUNCTION(BlueprintCallable)
	bool TryPurchase(int Price);


	UFUNCTION(BlueprintImplementableEvent)
	void BPPostPurchase();

	UFUNCTION(BlueprintImplementableEvent)
	void BPCallPowerBuilder(float Fossil, float Renewable);

	UFUNCTION(BlueprintCallable)
	int GetAP();

	UFUNCTION(BlueprintCallable)
	void SetAP(int Val);
	
	UFUNCTION(BlueprintCallable)
	int GetYear();

	UFUNCTION(BlueprintImplementableEvent)
	void CleanupPostTurn();
	
	virtual void BeginPlay() override;
	

	UFUNCTION(BlueprintCallable)
	void SetLilGuy(ACP_LilGuyBase* Guy);

	UFUNCTION(BlueprintCallable)
	ACP_LilGuyBase* GetLilGuy();

	void PrintGameModeVariables();
	
	UFUNCTION(BlueprintCallable)
	void CancelSelection();

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Classes")
	TSubclassOf<ACP_LilGuyBase> LilGuyClass;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Game Mode")
	FVector SpawnPoint;
	
	void SpawnLilGuy();

	UFUNCTION(BlueprintImplementableEvent)
	void PostGameStart();

	UPROPERTY(BlueprintReadWrite)
	bool FirstGuyRecruited = false;

	UFUNCTION(BlueprintCallable)
	TArray<ACP_LilGuyBase*>& GetLilGuyArray();

public:
	//Delegates
	FLilGuyChosenDelegate OnLilGuyChosen;
private:	
	// Main Gameplay Variables
	float CO2 = 0;
	float Profitability = 1;
	float Terrorism = 0;
	float LeftRight = 0.4; // 1 = Left, 0 = Right
	float RenewableEnergy = 0; //TBD
	float FossilFuelEnergy = 0;
	float Policy = 0;
	float AmountOfLilGuys = 4;
	
	TArray<ACP_LilGuyBase*> LilGuys;
	int Year = 0;
	int AP = 0;
	int MaxConstructionPerYear = 100;

	// The guy currently in focus by the camera
	UPROPERTY()
	ACP_LilGuyBase* CurrentlySelectedGuy;

	// Balance Parameters
	const float PolicyGrowthRate = 4;

};

